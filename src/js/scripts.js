(function ($) {
    "use strict";
    //VARIABLES
    var body = $("body"),
        header = $("#header");

    //PRELOADER FUNCTION
    $('#wrapper').on("click", ".nav-link", function (event) {
        event.preventDefault();
        var linkLocation = this.href;
        body.removeClass('ready-page');
        setTimeout(function () {
            window.location = linkLocation;
        }, 500);
    });
    setTimeout(function () {
        if ($(window).width() > 1280) {
            $('body').addClass('ready-page');
        }
    }, 0);

    // SLIDER DUNCTION
    if ($(".main-slider").length) {
        $(".main-slider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            arrows: true,
            fade: true,
            dots: true,
            responsive: [
                {
                    breakpoint: 1281,
                    settings: {
                        arrows: false,
                        fade: false
                    }
                }
            ]
        });
        $(".main-slider").on('afterChange', function () {
            $(".main-slider").find(".slick-current").addClass("zoom");
        });
    }
    if ($(".partners-list").length) {
        $(".partners-list").slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 0,
            cssEase: 'linear',
            speed: 8000,
            arrows: true,
            dots: false,
            responsive: [
                {
                    breakpoint: 1680,
                    settings: {
                        slidesToShow: 5,
                        arrows: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 1380,
                    settings: {
                        slidesToShow: 4,
                        arrows: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 780,
                    settings: {
                        slidesToShow: 3,
                        arrows: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 380,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                        dots: true
                    }
                }
            ]
        });
    }

    if ($(".achievements-gallery").length) {
        $(".achievements-gallery").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            arrows: true,
            dots: false,
            responsive: [
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                        dots: false
                    }
                },
                {
                    breakpoint: 380,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                        dots: false
                    }
                }
            ]
        });
    }
    if ($(".products-carousel").length) {
        $(".products-carousel").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            arrows: false,
            dots: true,
            appendDots: '.tabs-slider',
            responsive: [
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 780,
                    settings: {
                        slidesToShow: 1,
                        arrows: true,
                        dots: true
                    }
                }
            ]
        });
    }
    if ($(".foto-carousel").length) {
        $(".foto-carousel").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            arrows: true,
            dots: false,
            centerMode: true,
            responsive: [
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 780,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                        dots: true
                    }
                }
            ]
        });
    }
    if ($(".slider-for").length) {
        $(".slider-for").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            asNavFor: '.slider-nav',
            responsive: [
                {
                    breakpoint: 1280,
                    settings: {
                        arrows: false,
                        dots: false
                    }
                }
            ]
        });
    }
    if ($(".slider-nav").length) {
        $(".slider-nav").slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            arrows: true,
            fade: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 1280,
                    settings: {
                        arrows: true,
                        dots: false
                    }
                }
            ]

        });
    }
    if ($(".product-slider").length) {
        $(".product-slider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            arrows: false,
            dots: true
        });
    }

    jQuery(document).ajaxComplete(function() {
        if ($(".product-slider").length) {
            $(".product-slider").each(function() {
                if(!$(this).hasClass("slick-slider")) {
                    $(this).slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                        arrows: false,
                        dots: true
                    });
                    setTimeout(function () {
                      $(".ajax-wrap").addClass("show");
                    }, 500);
                }
            });
        }
    });

    //IMAGE PREVIEW
    if ($(".preview-list").length) {
       imagePreview();
    }
    function  imagePreview(){
        if ($(window).width() > 1280) {
            $(".product-list").find(".preview-list").on('mouseover', ".item" ,function () {
                var imageSrc = $(this).css("background-image");
                $(this).closest(".visual-wrap").find(".visual").css("background-image" , imageSrc);
            });
        }else{
            $(".product-list").find(".preview-list").on('click', ".item" , function () {
                var imageSrc = $(this).css("background-image");
                $(this).closest(".visual-wrap").find(".visual").css("background-image" , imageSrc);
            });
        }
    }

    if ($(".preview-section").length) {
        previewSection();
    }
    function  previewSection(){
        if ($(window).width() > 1280) {
            $(".preview-section").find(".visual-previews").on('mouseover', ".visual-preview" ,function () {
                var imageSrc = $(this).css("background-image");
                $(this).closest(".preview-section").find(".visual").css("background-image" , imageSrc);
            });
        }else{
            $(".preview-section").find(".visual-previews").on('click', ".visual-preview" , function () {
                var imageSrc = $(this).css("background-image");
                $(this).closest(".preview-section").find(".visual").css("background-image" , imageSrc);
            });
        }
    }

    //ALL RESIZE FUNCTIONS
    function resizeFunctions() {
        if ($(window).width() < 620) {
            //PRODUCTS INITIAL FUNCTION
            if ($(".tabs-header").length) {
                $(".tabs-header").slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    arrows: true,
                    dots: false
                });
                $('.tabs-header').on('afterChange', function (e) {
                    $('.tabs-header').find(".slick-current").click();
                });
            }
            if ($(".tabs-info-header").length) {
                $(".tabs-info-header").slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    arrows: true,
                    dots: false
                });
                $('.tabs-info-header').on('afterChange', function () {
                    $('.tabs-info-header').find(".slick-current").click();
                });
            }






        }
    }

    resizeFunctions();
    $(window).resize(function () {
        resizeFunctions();
    });

    // TABS FUNCTION
    if ($(".tabs-header").length) {
        tabsInit();
    }

    function tabsInit() {
        $(".tabs-slider , .tabs-info-header").find(".slick-dots").eq(0).addClass("active");
        var position;
        $('.tabs-header , .tabs-info-header').on("click", ".item", function () {
            position = $(this).index();
            $(this).addClass("active").siblings(".item").removeClass("active");
            $(this).closest(".section , .tabs").find(".tabs-body").find(".item-content").eq(position).addClass("active").siblings(".item-content").removeClass("active");
            $(this).closest(".section , .tabs").find(".tabs-slider").find(".slick-dots").eq(position).addClass("active").siblings(".slick-dots").removeClass("active");
        });
    }



    // KATEGORY-TABS FUNCTION
    if ($(".kategorien-tab").length) {
        kategorienTabsInit();
    }

    function kategorienTabsInit() {
        $('.kategorien-tab').on("click", ".visual", function () {
           $(this).closest(".col").addClass("active").siblings(".col").removeClass("active");
        });
    }

    // MOBILE MENU FUNCTION
    header.find(".menu-opener-js").on("click", function () {
        $(this).toggleClass("active").closest(header).toggleClass("menu-active");
    });
    $("#middle").on("click", function () {
        $(".menu-opener-js").removeClass("active").closest(header).removeClass("menu-active");
    });

    // FORM VALIDATION
    if ($("form").length) {
        formValidation();
    }

    function formValidation() {
        $("form").submit(function () {
            if ($(this).valid()) {
                return true;
            } else {
                return false;
            }
        });
        $('form').validate({
            errorPlacement: function (error, element) {
            }
        });
    }


    //FOOTER TABS
    if ($(window).width() < 992) {
        footerTab();
    }

    function footerTab() {
        $('.col-title').click(function (e) {
            $(this).next(".col-content").toggleClass("active");
        });
    }

    //jquery custom form
    $(function() {
        jcf.replaceAll();
    });


    if($(".accordeon-list").length){
        accordeonInit();
    }
    function accordeonInit(){
        $(".accordeon-list").find(".accordeon-item").on("click", ".header", function () {
            $(this).closest(".accordeon-item").toggleClass("active").siblings(".accordeon-item").removeClass("active");
        });
    }


    $(".iframe-visual").on("click", function () {
        $(".iframe-visual").addClass("active");
    });

    //AUTOSEARCH
    $(".search-form").on("input", "input", function () {
        var inputVal = $(this).val().length;
       if(inputVal >= 3){
           $(".search-form").addClass("search-result-show");
       }else {
           $(".search-form").removeClass("search-result-show");
       }
    });

    //SCROLL FUNCTION
    var h = $(window).height();
    boxAnimation();
    $(window).scroll(function () {

        if ($(".box").length) {
            boxAnimation();
        }
    });

    function boxAnimation() {
        $(".box").each(function () {
            if (($(window).scrollTop() + h + 100) >= $(this).offset().top) {
                $(this).find("span").addClass("animation");
            }
        });
    }

    $(window).scroll(function () {
        var h = $(window).height();
        scrollAnimation(h);
    });
    scrollAnimation();
    //scrollAnimation
    function scrollAnimation() {
        $(".section").each(function () {
            if (($(window).scrollTop() + $(window).height()) >= $(this).offset().top) {
                $(this).addClass("animation");
            }
        });
    }


    $(document).ready(function() {
        $('.fancybox').fancybox();
    });
    if($('#datepicker').length){
        $('#datepicker').datepicker();
    }

    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".ajax_search_response"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            $(".search-form").removeClass("search-result-show");
        }
    });






    if( $('.timepicker').length) {
        $('.timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            minTime: '10',
            maxTime: '6:00pm',
            defaultTime: '11',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
    }
    if( $('.post').length) {
        $('.post , .products-list').find(".col").each(function () {
            if ( $(this).find(".visual").length) {
                $(this).addClass("visual-wrap");
            }
        });
    }

    // DROP-DOWN INIT
    dropDownInit();

    function dropDownInit() {
        header.find(".drop-down-icon").on("click", function () {
            $(this).next(".dropdown-menu").toggleClass("active");
            $(this).closest(".navigation").toggleClass("drop-active");
        });
    }


    $('.hidden-text-action').on('click', function (e) {
        setTimeout(function () {
            $("html, body").animate({ scrollTop: $(document).height() }, "slow");
        }, 500);

    });
    $(".hidden-text").css("display" , "none");



})(jQuery);
